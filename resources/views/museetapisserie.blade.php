@extends('layouts.app')
@section('fond', 'musee')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <br><br>
            <div class="media">
                <img src="images/fil1.jpg" width="40%" class="align-self-start mr-3" alt="Musée tappiserie">
                <div class="media-body">
                    <h1 class="mt-0">Musée de la tapisserie d'Aubusson</h1>
                    <p>Le Musée de la tapisserie d'Aubusson est très connu de par son savoir-faire dans le domaine de la
                        tapisserie. Il a veut le conserver, enrichir et le mettre en valeur aux yeux du public.<br>
                        Il veut que cette richesse ne se perde pas dans le temps et c'est pour cela qu'il a décidé de
                        créer un évènement appelé "Sur le fil" pour présenter et montrer au public les savoir-faire et
                        engagements de la cité internationale de la tapisserie à la création contemporaine, donner un
                        regard nouveau sur la tapisserie et son histoire et élargir leurs cibles à tendance
                        vieillissante.</p>
                    <br>
                    <h2>MISSION</h2>
                    <p>Réalisation d'une affiche communiquant sur l'évènement "Sur le fil".</p>
                    <br>
                    <h2>CAHIER DES CHARGES</h2>
                    <p class="mb-0">* Créer une communication de l'exposition "Sur le fil".<br>
                        * Mettre en corrélation les oeuvres d'hier et d'aujourd'hui. <br>
                        * Offrir une immersion totale dans l'univers tissé d'Aubusson.<br>
                        * Annoncer que l'évènement aura lieu du 25 juin 2019 au 7 septembre 2019.</p>
                    <br>
                    <h2>CIBLE</h2>
                    <p>Personnes sensibles à l'art, à la création artisanale d'art, à l'histoire et les touristes.</p>
                </div>
            </div>
            <br><br><br>
            <h2>INCRUSTATIONS NUMÉRIQUES</h2>
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <!--Carousel Wrapper-->
                    <div id="carousel-example-2" class="carousel slide carousel-fade z-depth-1-half"
                        data-ride="carousel">
                        <!--Indicators-->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-2" data-slide-to="1"></li>
                            <li data-target="#carousel-example-2" data-slide-to="2"></li>
                            <li data-target="#carousel-example-2" data-slide-to="3"></li>
                        </ol>
                        <!--/.Indicators-->
                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="view">
                                    <img class="d-block w-100" src="images/fil2.jpg" alt="surlefil2">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/fil3.jpg" alt="surlefil3">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/fil4.jpg" alt="surlefil4">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/fil5.jpg" alt="surlefil5">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                            </div>
                        </div>
                        <!--/.Slides-->
                        <!--Controls-->
                        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <!--/.Controls-->
                    </div>
                    <!--/.Carousel Wrapper-->

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
