<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class CompteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        return view('auth.compte');
    }

    /**
     * Retourne le formulaire de mise à jour de l'adresse email
     *
     * @return void
     */
    public function emailUpdateForm()
    {
        return view('auth.compte-email');
    }

    /**
     * Mise à jour de l'adresse email de l'utilisateur après vérification des éléments
     *
     * @param Request $request
     * @return void
     */
    public function emailUpdate(Request $request)
    {

        $request->validate([
        'emailUpdate' => ['required', 'string', 'email', 'max:100', 'unique:users']
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $user->email = $request->emailUpdate;
        $user->save();

        notify()->info('Votre adresse email à bien été modifiée');
        return redirect('compte');
    }

    /**
     * Retourne la view pour éditer le pseudo
     *
     * @return void
     */
    public function pseudo()
    {
        return view('auth.compte-pseudo');
    }

    /**
     * Permet de modifier le pseudo de l'utilisateur
     * $request contient le pseudo que l'utilisateur à tapé
     *
     * @param Request $request
     * @return void
     */
    public function pseudoUpdate(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'min:5', 'max:50', 'unique:users'],
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $user->pseudo = $request->pseudo;
        $user->save();

        notify()->success('Votre pseudo à bien été modifié');
        return redirect('compte');
    }
}