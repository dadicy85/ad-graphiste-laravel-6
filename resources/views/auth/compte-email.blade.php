@extends('layouts.app')
@section('fond', 'contact')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 text-center">

            <p><strong>Mon adresse Mail:</strong> {{ Auth::user()->email }}</p>

            <div class="card-body">
                <form action="{{ route('compte.email') }}" method="POST">
                    @method('PATCH')
                    @csrf

                    <div class="form-group row">
                        <label for="emailUpdate"
                            class="col-md-4 col-form-label text-md-right">{{ __('New E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('emailUpdate') is-invalid @enderror"
                                name="emailUpdate" value="{{ old('emailUpdate') }}">

                            @error('emailUpdate')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                        <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">Changer mon adresse mail</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

