@extends('layouts.app')
@section('fond', 'musee')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <br><br>
            <div class="media">
                <img src="images/imp1.jpg" width="40%" class="align-self-start mr-3" alt="Musée tappiserie">
                <div class="media-body">
                    <h1 class="mt-0">Musée de l'imprimerie de Bordeaux</h1>
                    <p>Le Musée de l'Imprimerie souffre d'une image vieillissante auprès de la population bordelaise,
                        couplé d'une communication visuelle quasi inexistante et d'une identité visuelle qui ne
                        correspond plus à notre époque. De ce fait, le musée n'a pas vraiment de notoriété. Le bâtiment
                        tant dans son intérieur que son extérieur ne donne pas envie et rebute même les plus curieux. Le
                        manque d'évènements marquants pour faire connaitre le musée est aussi inexistant. En résumé, le
                        manque de notoriété du musée entraîne une méconnaissance de l'endroit et donc les spectateurs ne
                        peuvent pas se positionner et prendre une décision par rapport à ce qui existe déjà. Ils vont
                        être perturbés.
                        C'est pour cela qu'il est important de redorer l'image de l'imprimerie et d'essayer d'attirer à
                        nouveau bons nombres de personnes d'ages et d'horizons différents.</p>
                    <br>
                    <h2>MISSION</h2>
                    <p>Communiquer sur le musée afin qu'il gagne en notoriété pour qu'il puisse continuer à transmettre
                        le savoir-faire de l'imprimerie et donner envie aux Bordelais de s'intéresser à ce domaine.</p>
                    <br>
                    <h2>CAHIER DES CHARGES</h2>
                    <p class="mb-0">* Mettre en avant la mécanique des anciennes imprimantes.<br>
                        * Rajeunir l'image du domaine de l'imprimerie.<br>
                        * Donner envie à la cible de découvrir le musée.<br>
                        * Créer une communication qui aura pour objectif d'augmenter la notoriété du musée.</p>
                    <br>
                    <h2>CIBLE</h2>
                    <p>Toute personne étant intéressée de près ou de loin au domaine de l'impression, jeunes étudiants
                        dans l'art.</p>
                </div>
            </div>
            <br><br><br>
            <h2>INCRUSTATIONS NUMÉRIQUES</h2>
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <!--Carousel Wrapper-->
                    <div id="carousel-example-2" class="carousel slide carousel-fade z-depth-1-half"
                        data-ride="carousel">
                        <!--Indicators-->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-2" data-slide-to="1"></li>
                            <li data-target="#carousel-example-2" data-slide-to="2"></li>
                            <li data-target="#carousel-example-2" data-slide-to="3"></li>
                        </ol>
                        <!--/.Indicators-->
                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="view">
                                    <img class="d-block w-100" src="images/imp2.jpg" alt="imprimerie2">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/imp3.jpg" alt="imprimerie3">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/imp4.jpg" alt="imprimerie4">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/imp5.jpg" alt="imprimerie5">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                            </div>
                        </div>
                        <!--/.Slides-->
                        <!--Controls-->
                        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <!--/.Controls-->
                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
