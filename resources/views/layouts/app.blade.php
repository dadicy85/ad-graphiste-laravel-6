<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Créations Web et graphiques. Sites Web, Logos, Flyers."/>
    <meta name="keywords" content="Design, Créations, Web">
    <meta name="author" content="Dadicy">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'AD Graphiste - Design & Créations') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body id="@yield('fond')">

    <div id="page">
        <!-- Début Navbar -->
        <div class="navbar-area">
            <!-- Menu Version Mobile -->
            <div class="mobile-nav">
                <a href="/accueil" class="logo">
                    <img src="images/logoad.png" width="50px" height="34px" alt="AD Graphiste">
                </a>
            </div>

            <!-- Menu For Desktop Device -->
            <div class="main-nav">
                <div class="container">
                    <nav class="navbar navbar-expand-md navbar-light">
                        <a class="navbar-brand" href="/accueil">
                            <img src="/images/logoad.png" width="90px" height="63px" alt="AD Graphiste">
                        </a>
                        <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="/accueil" class="nav-link">Accueil</a>
                                </li>
                                <li class="nav-item">
                                    <a href="/presentation" class="nav-link">Qui suis-je ?</a>
                                </li>
                                <li class="nav-item">
                                    <a href="/services" class="nav-link">Services</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link dropdown-toggle">Galeries</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="/galeriepro" class="nav-link">Professionnelle</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/galerieperso" class="nav-link">Personnelle</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/affiches" class="nav-link">Affiches Publicitaires</a>
                                        </li>

                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="/contact" class="nav-link">Contact</a>
                                </li>

                                {{-- Le menu déroulant des infos personneslles ne sera visible que si l'user est authentifié --}}
                                <li class="nav-item">
                                    @auth
                                    <a href="#" class="nav-link dropdown-toggle">Mon Espace</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="{{ route('compte') }}" class="nav-link">Informations Personnelles</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">Supprimer mon compte</a>
                                        </li>
                                        @if(Auth()->user()->is_admin==1)
                                        <hr>
                                        <li class="nav-item">
                                            <a href="/admin" class="nav-link">Administration</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/upload-file" class="nav-link">Upload Documents</a>
                                        </li>
                                        @endif
                                        @endauth
                                        @guest
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                        @if (Route::has('register'))
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                        </li>
                                        @endif
                                        @else
                                        <li class="nav-item dropdown">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                                {{-- Script "Bonjour/Bonsoir" selon heure de connexion de l'user --}}

                                                <span id="bonjour">Bonjour</span>
                                                <script type="text/javascript">
                                                    today = new Date()
                                                    if (today.getHours() >= 0 && today.getHours() < 18) {
                                                        /* A noter que si on mettait <= Bonjour continuerait jusqu'à 18h59 et ici, on souhaite qu' il s'arrête à 18h */
                                                        document.getElementById('bonjour').innerHTML = 'Bonjour';
                                                    } else {
                                                        document.getElementById('bonjour').innerHTML = 'Bonsoir';
                                                    }
                                                </script> {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                    style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </li>
                                        @endguest
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Fin Navbar -->
        <!-- JQuery Min JS -->
<br><br>
        <div id='content' class="py-4" style="margin-top: 5%; margin-bottom: 5%">
            @yield('content')
        </div>
 
        {{-- Cookie --}}
        @include('cookieConsent::index')

        <!-- Début du Footer -->
            <div class="footer-copyright text-center py-3">
                <!-- Réseaux Sociaux -->
                <ul class="list-unstyled list-inline text-center">
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/adgraphistee/?modal=admin_todo_tour" target="_blank">
                            <i class="fab fa-facebook-f fa-2x"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.instagram.com/adgraphiste/?hl=fr" target="_blank">
                            <i class="fab fa-instagram fa-2x"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.linkedin.com/in/alice-dacquin-67232a1b0/" target="_blank">
                            <i class="fab fa-linkedin-in fa-2x"> </i>
                        </a>
                    </li>
                </ul>
                © 2020 Copyright:
                <a href="/accueil" class="copy"> ADGraphiste</a><br>
                <a href="/cgv" class="copy">Conditions Générales de Ventes</a>
            </div>
        <!-- Fin du Footer -->
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <!-- MeanMenu JS -->
        <script
            src="https://techsolutionshere.com/wp-content/themes/techsolution/assets/blog-post-css-js/jquery.meanmenu.js">
        </script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.js"></script>
        <script src="/js/lity.min.js"></script>
        <script>
            // Mean Menu
            jQuery('.mean-menu').meanmenu({
                meanScreenWidth: "991"
            });
        </script>
</body>
</html>

