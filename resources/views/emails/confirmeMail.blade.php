@extends('layouts.app')
@section('fond', 'confirmeMail')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div class="media">
            <img src="images/letter.png" width="40%" class="align-self-start mr-3" alt="Message envoyé">
            <div class="media-body">
                <p><span id="bonjour">Bonjour</span></p>

                <script type="text/javascript"> //Bonjour ou bonsoir selon l'heure de la journée
                    today = new Date()
                    if (today.getHours() >= 0 && today.getHours() < 18) {
                        document.getElementById('bonjour').innerHTML = 'Bonjour,';
                    } else {
                        document.getElementById('bonjour').innerHTML = 'Bonsoir,';
                    }
                </script><br />

                <p class="mb-0">Votre message a bien été envoyé à notre équipe.</p>
                <p class="mb-0">Nous reviendrons vers vous dans les plus brefs délais.</p>
                <br />
                <p>Merci pour votre confiance.</p>
                <p><b>AD Graphiste</b></p>
                <br />
                <p><a href="/accueil">Retour à l'accueil</a></p>
               </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
