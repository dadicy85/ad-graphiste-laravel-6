<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function accueil()
    {
        return view('accueil');
    }

    public function affiches()
    {
        return view('affiches');
    }

    public function museetapisserie()
    {
        return view('museetapisserie');
    }

    public function lemondejeunes()
    {
        return view('lemondejeunes');
    }

    public function festival()
    {
        return view('festival');
    }

    public function museeimprimerie()
    {
        return view('museeimprimerie');
    }

    public function pro()
    {
        return view('galeriepro');
    }

    public function perso()
    {
        return view('galerieperso');
    }

    public function presentation()
    {
        return view('presentation');
    }

    public function commandes()
    {
        return view('commandes');
    }

    public function factures()
    {
        return view('factures');
    }

    public function services()
    {
        return view('services');
    }

    public function contact()
    {
        return view('contact');
    }

    public function cgv()
    {
        return view('cgv');
    }

    public function contactMail()
    {
        return view('contactMail');
    }

    public function confirmeMail()
    {
        return view('confirmeMail');
    }
}
