<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galerieperso extends Model
{
    protected $table = 'galeriepersos';

    protected $fillable = [
        'file'
    ];
}
