@extends('layouts.app')
@section('fond', 'pres')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <img src="images/qui1.jpg" width="100%" style="transform: translate(0px, -26px)" ;>
            <br><br>
            <h1 style="text-align: center">Alice DACQUIN</h1>
            <strong><h2 style="text-align: center">Infographiste</h2></strong>
            <br><br>
            <div class="row justify-content-center">
                <div class="col-md-2" style="width: 50px;">
                    <img class="img-fluid img-thumbnail logopres" src="images/logo1.png" alt="Identités visuelles"></a>
                    <div class="textpres">Identités visuelles</div>
                </div>
                <div class="col-md-2" style="width: 50px;">
                    <img class="img-fluid img-thumbnail logopres" src="images/logo2.png" alt="Blason"></a>
                    <div class="textpres">Blason</div>
                </div>
                <div class="col-md-2" style="width: 50px;">
                    <img class="img-fluid img-thumbnail logopres" src="images/logo3.png" alt="Chartes graphique"></a>
                    <div class="textpres">Charte graphique</div>
                </div>
                <div class="col-md-2" style="width: 50px;">
                    <img class="img-fluid img-thumbnail logopres" src="images/logo4.png" alt="Catalogues"></a>
                    <div class="textpres">Catalogues</div>
                </div>
                <div class="col-md-2" style="width: 50px;">
                    <img class="img-fluid img-thumbnail logopres" src="images/logo5.png" alt="Flyers"></a>
                    <div class="textpres">Flyers</div>
                </div>
                <div class="col-md-2" style="width: 50px;">
                    <img class="img-fluid img-thumbnail logopres" src="images/logo6.png" alt="Supports Imprimés"></a>
                    <div class="textpres">Supports Imprimés</div>
                </div>
            </div>
            <br><br>
            <p style="text-align: center; font-size: 18px">Pour plus de renseignements, n'hésitez par à vous référer à la section <strong><a
                        href="/services">services.</a></strong>
            </p>
            <br><br>
            <div>
                <h2>INFOGRAPHISTE A VOTRE SERVICE</h2>
            </div>

            <div class="row">
                <div class="col-md-7">

                    <p>Mon but ? <br>
                        Aider les entreprises, nouvelles comme anciennes, à créer ou renouveler leur identité
                        graphique, tant dans leur logo que leurs moyens de communication pour les démarquer de leurs
                        concurrents. Etant spécialisée dans la conception de divers supports imprimés et/ou numériques,
                        je vous
                        propose le meilleur pour votre entreprise en terme de graphisme :</p>

                    <ul>
                        <li><strong>Pas d'identité visuelle pour votre entreprise naissante ?</strong></li>
                        <li><strong>Volonté de renouveler votre image de marque auprès de vos consommateurs ?</strong>
                        </li>
                        <li><strong>Volonté de créer des supports imprimés pour promouvoir votre marque ?</strong></li>
                        <li><strong>Une illustration ou une mise en page à faire ?</strong></li>
                    </ul>
                    <p>Je met à disposition mes connaissances et capacités graphiques pour répondre et m'adapter à la
                        demande de
                        votre entreprise.</p>
                </div>
                <br>
                <div class="col-md-5">
                    <img src="images/qui2.jpg" width="100%" alt="presentation2">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
