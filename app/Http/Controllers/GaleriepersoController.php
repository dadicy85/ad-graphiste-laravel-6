<?php

namespace App\Http\Controllers;

use App\Galerieperso;
use Illuminate\Filesystem\FilesystemManager as Storage;
use Illuminate\Http\Request;

class GaleriepersoController extends Controller
{
    public function index (){
       return view('galerieperso');
    }

    public function store (Request $request){
       $this->validate($request, [
           'file' => 'required|mimes:bmp,png,jpg,jpeg|max:2048'
       ]);
       $file = $request->file('file');
       $name = time() . $file->getClientOriginalName();
       $file->move('uploadsperso', $name);
       Galerieperso::create([
           "file" => "/uploadsperso/{$name}"
       ]);
       return "Image(s) envoyée(s) avec succès !";
    }

    public function show (){
        $galeriepersos = Galerieperso::paginate(8);
        return view('galerieperso', compact('galeriepersos'));
    }

    public function destroy($file)
    {
        $galeriepersos = Galerieperso::where('id', $file)->first();

        if ($galeriepersos != null) 
        {
            $galeriepersos->delete();
            return redirect()->route('galerieperso')->with(['message'=> 'Successfully deleted!!']);
        }
         return redirect()->route('galerieperso')->with(['message'=> 'Wrong ID!!']);

    }
}
