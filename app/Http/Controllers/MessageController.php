<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\contactMail;

class MessageController extends Controller
{
    public function create()
    {
        return view('contactMail');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|max:255',
            'email' => 'required|email|min:10|max:255',
            'message' => 'required|min:20|max:1000'
        ]);


        Mail::to('ad.graphiste.33@gmail.com')
            ->send(new contactMail($request->except('_token')));

        return view('emails.confirmeMail');
    }

}
