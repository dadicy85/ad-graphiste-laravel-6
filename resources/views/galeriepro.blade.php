@extends('layouts.app')
@section('fond', 'pro')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 text-center">

            <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Œuvres Professionnelles</h1><br>
            <hr>
            <div class="row">
                @foreach($galeriepros as $galeriepro)
                <div class="col-lg-3 col-md-4 col-6" style="width: 20rem;">
                    <a href="{{ $galeriepro->file }}" data-lity class="d-block mb-4 h-100"><img
                            class="img-fluid img-thumbnail" src="{{ $galeriepro->file }}" alt="..."></a>
                </div>
                @endforeach
            </div>
            {{ $galeriepros->links() }}
            @auth
            @if(Auth()->user()->is_admin==1)
            <br>
            <div style="margin-bottom: 3px; display: flex; justify-content: flex-end">
                <a class="btn btn-outline-info" href="{{ url('/galeriepro') }}">Charger les images</a>
            </div>
            <br>
            <div>
                <form action="{{ url('galeriepro') }}" class="dropzone">
                    {{ csrf_field() }}
                </form>
            </div>
            @endif
            @endauth
        </div>
    </div>
</div>
@endsection
