<div class="js-cookie-consent cookie-consent">

    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>
    &nbsp;
    <button class="accept js-cookie-consent-agree cookie-consent__agree">
        {{ trans('cookieConsent::texts.agree') }}
    </button>
    &nbsp;
    &nbsp;
    <a href="https://www.cnil.fr/fr/cookies-et-traceurs-que-dit-la-loi">
    <button class="info">
        En savoir plus
    </button>
</a>
</div>
