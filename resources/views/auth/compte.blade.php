@extends('layouts.app')
@section('fond', 'cgv')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h3 class="section-title">Bienvenue {{ Auth::user()->name }} dans votre Espace Personnel</h3>
            <br><br>
            <table class="table table-profile">
                <thead>
                    <tr>
                        <th colspan="2">CIVILITÉ</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                        <td class="field">Pseudo</td>
                        <td class="value">
                            {{ Auth::user()->name }}
                            <a href="#" class="m-l-10">Modifier</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="field">Nom</td>
                        <td class="value">
                            {{ Auth::user()->lastname }}
                            <a href="#" class="m-l-10">Modifier</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="field">Prénom</td>
                        <td class="value">
                            {{ Auth::user()->firstname }}
                            <a href="#" class="m-l-10">Modifier</a>
                        </td>
                    </tr>
                    <thead>
                        <tr>
                            <th colspan="2">CONTACT</th>
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td class="field">Téléphone</td>
                        <td class="value">
                            {{ Auth::user()->phone }}
                            <a href="#" class="m-l-10">Modifier</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="field">Email</td>
                        <td class="value">
                            {{ Auth::user()->email }}
                            <a href="{{ route('compte-email') }}" class="m-l-10">Modifier</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="field">Société</td>
                        <td class="value">
                            {{ Auth::user()->society }}
                            <a href="#" class="m-l-10">Modifier</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
