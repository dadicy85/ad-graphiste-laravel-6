<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/accueil', function () {
    return view('accueil');
});

Route::get('/presentation', function () {
    return view('/presentation');
});

Route::get('/affiches', function () {
    return view('/affiches');
});

Route::get('/museetapisserie', function () {
    return view('/museetapisserie');
});

Route::get('/lemondejeunes', function () {
    return view('/lemondejeunes');
});

Route::get('/festival', function () {
    return view('/festival');
});

Route::get('/museeimprimerie', function () {
    return view('/museeimprimerie');
});

Route::get('/services', function () {
    return view('/services');
});

Route::get('/contact', function () {
    return view('/contact');
});

Route::get('/cgv', function () {
    return view('/cgv');
});


// Route envoi message de la vue Contact
Route::post('message', 'MessageController@store')->name('message');

// Route Gallerie Pro
Route::get('galeriepro', 'GalerieproController@index');
Route::post('galeriepro', 'GalerieproController@store');
Route::get('galeriepro', 'GalerieproController@show');


// Route Gallerie Perso
Route::get('galerieperso', 'GaleriepersoController@index');
Route::post('galerieperso', 'GaleriepersoController@store');
Route::get('galerieperso', 'GaleriepersoController@show');
Route::delete('galerieperso/{id}', 'GaleriepersoController@destroy')->name('galerieperso.delete');


// Routes Auth
Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    
    Route::get('compte', 'CompteController@index')->name('compte');
    
    Route::get('compte/email', 'CompteController@emailUpdateForm')->name('compte-email');
    Route::patch('compte/email', 'CompteController@emailUpdate')->name('compte.email');
    Route::get('compte/pseudo', 'CompteController@pseudo')->name('compte.pseudo');
    Route::patch('compte/pseudo', 'CompteController@pseudoUpdate')->name('compte.pseudo.update');
    Route::delete('compte/delete', 'CompteController@destroy')->name('compte.delete');
});


// Routes Admin
Route::group(['prefix' => 'admin'], function () {
Voyager::routes();
    Route::get('/admin')->middleware('admin');

});

// Route Upload Document
Route::get('/upload-file', 'FileUpload@createForm');

// Route Stockage Document
Route::post('/upload-file', 'FileUpload@fileUpload')->name('fileUpload');