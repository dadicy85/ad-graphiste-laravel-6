@extends('layouts.app')
@section('fond', 'perso')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 text-center">

            <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Œuvres Personnelles</h1><br>
            <hr>
            <div class="row">
                @foreach($galeriepersos as $galerieperso)
                <div class="col-lg-3 col-md-4 col-6" style="width: 20rem;">
                    <a href="{{ $galerieperso->file }}" data-lity class="d-block mb-4 h-100"><img
                            class="img-fluid img-thumbnail" src="{{ $galerieperso->file }}" alt="..."></a>
                </div><br>
                         <div>
                          <form action="{{ route('galerieperso.delete')}}" method="POST" class="w-50 centre row" enctype="multipart/form-data">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger mt-3" type="submit">Supprimer</button>
                                        </form>
</div>
                @endforeach
            </div>
            {{ $galeriepersos->links() }}
            @auth
            @if(Auth()->user()->is_admin==1)
            <br>
            <div style="margin-bottom: 3px; display: flex; justify-content: flex-end">
                <a class="btn btn-outline-info" href="{{ url('/galerieperso') }}">Charger les images</a>
            </div>
            <br>
            <div>
                <form action="{{ url('galerieperso') }}" class="dropzone">
                    {{ csrf_field() }}
                </form>
            </div>
            @endif
            @endauth
        </div>
    </div>
</div>
@endsection
