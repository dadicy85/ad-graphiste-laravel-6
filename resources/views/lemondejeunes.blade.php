@extends('layouts.app')
@section('fond', 'monde')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <br><br>
            <div class="media">
                <img src="images/monde1.jpg" width="40%" class="align-self-start mr-3" alt="Musée tappiserie">
                <div class="media-body">
                    <h1 class="mt-0">Offre abonnement jeune Le Monde.fr</h1>
                    <p>Les jeunes ne sont pas sensibles à la presse numérique. De ce fait, Le Monde veut les motiver à
                        aller sur le site ce qui rendra son public plus jeune.
                        Il veut rajeunir son image auprès des jeunes de 18-25 ans en leur proposant une offre
                        promotionnelle qui pourrait les intéresser de par son faible coût.</p>
                    <br>
                    <h2>MISSION</h2>
                    <p>Communiquer et informer sur l'offre promotionnelle 100% numérique et sur son prix abordable de
                        tous sur un support web.</p>
                    <br>
                    <h2>CAHIER DES CHARGES</h2>
                    <p class="mb-0">* Communiquer sur une offre promotionnelle.<br>
                        * Attiser l'interêt des jeunes pour la presse numérique.<br>
                        * Utilisation de codes/symboles graphiques propre aux jeunes.<br>
                        * Prix et logo LeMonde.fr doivent être obligatoirement apparents.</p>
                    <br>
                    <h2>CIBLE</h2>
                    <p>18 - 25 ans</p>
                </div>
            </div>
            <br><br><br>
            <h2>INCRUSTATIONS NUMÉRIQUES</h2>
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <!--Carousel Wrapper-->
                    <div id="carousel-example-2" class="carousel slide carousel-fade z-depth-1-half"
                        data-ride="carousel">
                        <!--Indicators-->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-2" data-slide-to="1"></li>
                            <li data-target="#carousel-example-2" data-slide-to="2"></li>
                            <li data-target="#carousel-example-2" data-slide-to="3"></li>
                        </ol>
                        <!--/.Indicators-->
                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="view">
                                    <img class="d-block w-100" src="images/monde2.jpg" alt="lemondejeune2">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/monde3.jpg" alt="lemondejeune3">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/monde4.jpg" alt="lemondejeune4">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/monde5.jpg" alt="lemondejeune5">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                            </div>
                        </div>
                        <!--/.Slides-->
                        <!--Controls-->
                        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <!--/.Controls-->
                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
