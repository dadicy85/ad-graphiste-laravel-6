<?php

namespace App\Http\Controllers;

use App\Galeriepro;
use Illuminate\Http\Request;

class GalerieproController extends Controller
{
    public function index (){
       return view('galeriepro');
    }

    public function store (Request $request){
       $this->validate($request, [
           'file' => 'required|mimes:bmp,png,jpg,jpeg|max:2048'
       ]);
       $file = $request->file('file');
       $name = time() . $file->getClientOriginalName();
       $file->move('uploadspro', $name);
       Galeriepro::create([
           "file" => "/uploadspro/{$name}"
       ]);
       return "Image(s) envoyée(s) avec succès !";
    }

    public function show (){
        $galeriepros = Galeriepro::paginate(8);
        return view('galeriepro', compact('galeriepros'));
    }
}
