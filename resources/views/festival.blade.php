@extends('layouts.app')
@section('fond', 'fest')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <br><br>
            <div class="media">
                <img src="images/fest1.jpg" width="40%" class="align-self-start mr-3" alt="Musée tappiserie">
                <div class="media-body">
                    <h1 class="mt-0">Festival de musique électronique</h1>
                    <p>Dans le cadre de mes études, je devais réaliser une affiche pour un festival de musique
                        électronique tenant place à Bordeaux en 2019 en analysant des formes et des symboles propres au
                        domaine de la cartographie.
                        Cette analyse avait pour but de me permettre de relever de nombreux styles de symboles, de me
                        les approprier personnellement et de retranscrire l'univers musical à travers ma propre
                        perception de la sonorité et de la musique.</p>
                    <br>
                    <h2>MISSION</h2>
                    <p>Créer une communication visuelle faisant la promotion et présentant ce festival aux yeux de la
                        population.</p>
                    <br>
                    <h2>CAHIER DES CHARGES</h2>
                    <p class="mb-0">* Communiquer dans le domaine de l'évenamentiel.<br>
                        * Traduire graphiquement un univers musical particulier.(Musique électro)<br>
                        * Donner envie à la cible de découvrir le musée.<br>
                        * Indique les artistes participant au festival.<br>
                        * Annoncer la date et le lieu où se déroule le festival.</p>
                    <br>
                    <h2>CIBLE</h2>
                    <p>Personne intéressée par la musique électronique et les curieux de ce domaine musical.</p>
                </div>
            </div>
            <br><br><br>
            <h2>INCRUSTATIONS NUMÉRIQUES</h2>
            <br><br>
            <div class="row">
                <div class="col-md-12">

                    <!--Carousel Wrapper-->
                    <div id="carousel-example-2" class="carousel slide carousel-fade z-depth-1-half"
                        data-ride="carousel">
                        <!--Indicators-->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-2" data-slide-to="1"></li>
                            <li data-target="#carousel-example-2" data-slide-to="2"></li>
                            <li data-target="#carousel-example-2" data-slide-to="3"></li>
                        </ol>
                        <!--/.Indicators-->
                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="view">
                                    <img class="d-block w-100" src="images/fest2.jpg" alt="festival2">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/fest3.jpg" alt="festival3">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/fest4.jpg" alt="festival4">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <!--Mask color-->
                                <div class="view">
                                    <img class="d-block w-100" src="images/fest5.jpg" alt="festival5">
                                    <div class="mask rgba-black-light"></div>
                                </div>
                            </div>
                        </div>
                        <!--/.Slides-->
                        <!--Controls-->
                        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <!--/.Controls-->
                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
