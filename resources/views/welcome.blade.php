<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Créations Web et graphiques. Sites Web, Logos, Flyers."/>
        <meta name="keywords" content="Design, Créations, Web">
        <meta name="author" content="Dadicy">

        <title>Bienvenue sur AD Graphiste</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {

                background-image: url("../images/adfond2.jpg");
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                background-size: cover;
                background-position: center;
                background-image:
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }


            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 75px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links > a:hover {
                color: red;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/accueil') }}">{{ __('Home') }}</a>
                    @else
                        <a href="{{ url('/accueil') }}">{{ __('Home') }}</a>
                        <a href="{{ route('login') }}">{{ __('Login') }}</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">{{ __('Register') }}</a>

                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">


                </div>
            </div>
        </div>
    </body>
</html>
