@extends('layouts.app')
@section('fond', 'services')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <section id="team" class="pb-5">
                <div class="container">
                    <h5 class="section-title h1">NOS SERVICES</h5><br>
                    <div class="row">
                        <!-- Item 1 -->
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                <div class="mainflip">
                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p><img class="img-fluid" src="/images/logo1.png" alt="Logo"></p>
                                                <br>
                                                <h4 class="card-title">LOGO</h4><br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="backside">
                                        <div class="card">
                                            <div class="card-body mt-4">
                                                <p class="card-text">Création d'un symbôle graphique qui a pour but de
                                                    représenter l'univers de votre domaine d'activité.</p><br>
                                                <ul>
                                                    <li><strong>Création du symbôle.</strong></li>
                                                    <li><strong>Choix typographique et colorimétrique.</strong></li>
                                                    <li><strong>Optimisation et finalisation avec le client.</strong>
                                                    </li>
                                                    <li><strong>Livraisons sous plusieurs formats.</strong></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Fin Item 1 -->
                        <!-- Item 2 -->
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                <div class="mainflip">
                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <img class="img-fluid" src="/images/logo2.png" alt="Blason">
                                                <br><br><br>
                                                <h4 class="card-title">BLASON</h4><br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="backside">
                                        <div class="card">
                                            <div class="card-body mt-4">
                                                <p class="card-text">Création d'un emblème ou armoirie graphique qui a
                                                    pour but de représenter une qualité de votre entreprise ou son
                                                    appartenance à un groupe.</p><br>
                                                <ul>
                                                    <li><strong>Création de l'emblème.</strong></li>
                                                    <li><strong>Choix typographique et colorimétrique.</strong></li>
                                                    <li><strong>Optimisation et finalisation avec le client.</strong>
                                                    </li>
                                                    <li><strong>Livraisons sous plusieurs formats.</strong></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Fin Item 2 -->
                        <!-- Item 3 -->
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                <div class="mainflip">
                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p><img class="img-fluid" src="/images/logo6.png"
                                                        alt="Supports Imprimés"></p>
                                                <br>
                                                <h4 class="card-title">SUPPORTS IMPRIMÉS</h4><br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="backside">
                                        <div class="card">
                                            <div class="card-body mt-4">
                                                <p class="card-text">Mise en page de vos documents imprimés sous forme
                                                    de maquettes</p>
                                                <p style="color: red"><strong>Papeterie</strong></p>
                                                <p class="card-text">Cartes de visite, Flyers, Affiches, Catalogues,
                                                    etc...</p>
                                                <p style="color: red"><strong>Impression</strong></p>
                                                <p class="card-text">Nous vous accompagnons pour vous aider à trouver
                                                    les meilleures entreprises d'impression afin de sublimer vos
                                                    supports de communication.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Fin Item 3 -->
                        <!-- Item 4 -->
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                <div class="mainflip">
                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <p><img class="img-fluid" src="/images/logo3.png"
                                                        alt="Charte Graphique"></p>
                                                <br>
                                                <h4 class="card-title">CHARTE GRAPHIQUE</h4><br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="backside">
                                        <div class="card">
                                            <div class="card-body mt-4">
                                                <p class="card-text">La charte graphique est un guide comprenant les
                                                    recommandations d'utilisation et les caractéristiques des différents
                                                    éléments graphiques
                                                    qui peuvent être utilisés sur les différents supports de
                                                    communication de l'entreprise.
                                                    Elle permet de garantir l'homogénéité et la cohérence de la
                                                    communication visuelle au sein et en dehors de l'entreprise.</p>
                                                <ul>
                                                    <li><strong>Présentation du principe du logo.</strong></li>
                                                    <li><strong>Choix Typographique et colorimétrique.</strong></li>
                                                    <li><strong>Guide d'utilisation, démonstration et application sous
                                                            plusieurs formats et supports.</strong></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Fin Item 4 -->
            </section>
        </div>
    </div>
</div>
@endsection
