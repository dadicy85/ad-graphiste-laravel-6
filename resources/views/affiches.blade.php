@extends('layouts.app')
@section('fond', 'affiches')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <h1>AFFICHES</h1>
            <hr><br>
            <div class="media">
                <img src="images/fil5.jpg" width="40%" class="align-self-start mr-3" alt="Musée tappiserie">
                <div class="media-body">
                    <h3 class="mt-0">Musée de la tapisserie d'Aubusson</h3>
                    <p>Le Musée de la tapisserie d'Aubusson est très connu de par son savoir-faire dans le domaine de la
                        tapisserie. Il a veut le conserver, enrichir et le mettre en valeur aux yeux du public.</p><br>
                    <p class="mb-0">Il veut que cette richesse ne se perde pas dans le temps et c'est pour cela qu'il a
                        décidé de créer un évènement appelé "Sur le fil" pour présenter et montrer au public les
                        savoir-faire
                        et engagements de la cité internationale de la tapisserie à la création contemporaine, donner un
                        regard nouveau sur la tapisserie et son histoire et élargir leurs cibles à tendance
                        vieillissante. (...)</p>
                    <br>
                    <p style="margin-bottom: 3px; display: flex; justify-content: flex-end"><a
                            class="btn btn-outline-info" href="/museetapisserie">En savoir plus</a></p>
                </div>
            </div>
            <br>
            <hr><br>
            <div class="media">
                <img src="images/monde5.jpg" width="40%" class="align-self-start mr-3" alt="monde jeunes">
                <div class="media-body">
                    <h3 class="mt-0">Offre abonnement jeune Le Monde.fr</h3>
                    <p>Les jeunes ne sont pas sensibles à la presse numérique. De ce fait, Le Monde veut les motiver à
                        aller sur le site ce qui rendra son public plus jeune.</p><br>
                    <p class="mb-0">Il veut rajeunir son image auprès des jeunes de 18-25 ans en leur proposant une
                        offre promotionnelle qui pourrait les intéresser de par son faible coût. (...)</p>
                    <br>
                    <p style="margin-bottom: 3px; display: flex; justify-content: flex-end"><a
                            class="btn btn-outline-info" href="/lemondejeunes">En savoir plus</a></p>
                </div>
            </div>
            <br>
            <hr><br>
            <div class="media">
                <img src="images/fest5.jpg" width="40%" class="align-self-start mr-3" alt="Festival">
                <div class="media-body">
                    <h3 class="mt-0">Festival de musique électronique</h3>
                    <p>Dans le cadre de mes études, j'ai réalisé une affiche pour un festival de musique électronique
                        tenant place à Bordeaux en 2019 en analysant des formes et des symboles propres au domaine de la
                        cartographie.</p><br>
                    <p class="mb-0">Cette analyse avait pour but de me permettre de relever de nombreux styles de
                        symboles, de me les approprier personnellement et de retranscrire l'univers musical à travers ma
                        propre perception de la sonorité et de la musique. (...)</p>
                    <br>
                    <p style="margin-bottom: 3px; display: flex; justify-content: flex-end"><a
                            class="btn btn-outline-info" href="/festival">En savoir plus</a></p>
                </div>
            </div>
            <br>
            <hr><br>
            <div class="media">
                <img src="images/imp5.jpg" width="40%" class="align-self-start mr-3" alt="Musée imprimerie">
                <div class="media-body">
                    <h3 class="mt-0">Musée de l'imprimerie de Bordeaux</h3>
                    <p>Le Musée de l'Imprimerie souffre d'une image vieillissante auprès de la population bordelaise,
                        couplé d'une communication visuelle quasi inexistante et d'une identité visuelle qui ne
                        correspond plus à notre époque. De ce fait, le musée n'a pas vraiment de notoriété.
                        Le bâtiment, tant dans son intérieur que son extérieur ne donne pas envie et rebute même les
                        plus curieux. Le manque d'évènements marquants pour faire connaitre le musée est aussi
                        inexistant.</p>
                    <p class="mb-0">En résumé, le manque de notoriété du musée entraîne une méconnaissance de l'endroit
                        et donc les spectateurs ne peuvent pas se positionner et prendre une décision par rapport à ce
                        qui existe déjà. Ils vont être perturbés.
                        C'est pour cela qu'il est important de redorer l'image de l'imprimerie et d'essayer d'attirer à
                        nouveau bons nombres de personnes d'âges et d'horizons différents. (...)</p>
                    <br>
                    <p style="margin-bottom: 3px; display: flex; justify-content: flex-end"><a
                            class="btn btn-outline-info" href="/museeimprimerie">En savoir plus</a></p>
                </div>
            </div>
            <br>
            <hr>
        </div>
    </div>
</div>
@endsection
