<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeriepro extends Model
{
    protected $table = 'galeriepros';

    protected $fillable = [
        'file'
    ];
}
